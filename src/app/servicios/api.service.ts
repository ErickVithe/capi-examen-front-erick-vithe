import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest } from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  URL = "http://127.0.0.1:8000/";

  constructor(private http: HttpClient) { }

  getUsuarios() {
    return this.http.get(`${this.URL}users`);
  }
}