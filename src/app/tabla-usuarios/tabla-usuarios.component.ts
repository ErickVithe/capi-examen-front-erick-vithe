import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';  
import { ApiService } from './../servicios/api.service';

@Component({
  selector: 'app-tabla-usuarios',
  templateUrl: './tabla-usuarios.component.html',
  styleUrls: ['./tabla-usuarios.component.css']
})
export class TablaUsuariosComponent implements OnInit {
  users: any = [];

  constructor(private router: Router, private api: ApiService) { }

  ngOnInit(): void {
    this.api.getUsuarios().subscribe((data:any)=>{
      this.users = data;
      console.log(this.users);
    });
  }

}
